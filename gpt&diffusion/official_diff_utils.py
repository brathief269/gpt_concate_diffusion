from diffusers import StableDiffusionPipeline
import torch

def official_diff_loader(model_id = 'runwayml/stable-diffusion-v1-5'):
    print('using official diffusion: ')
    pipe = StableDiffusionPipeline.from_pretrained(model_id, device_map = None, torch_dtype=torch.float16).to('cuda')
    pipe = pipe.to("cuda")
    pipe = pipe.to("cuda")
    pipe = pipe.to("cuda")
    return pipe

def acce_diff_load():
    pass


def official_diff_gen(
        prompt_from_gpt,
        pipe,
        num_inference_steps = 30,
        guidance_scale = 7.5,
        multi_img = False,
        num_images = 1,
    ):
    if not multi_img:
        i = 1
        for prompt in prompt_from_gpt:
            image = pipe(
                prompt,
                num_inference_steps = num_inference_steps,
                guidance_scale = guidance_scale,
            ).images[0]  

            image.save(f"/content/drive/MyDrive/vscode_python/gpt&diffusion/output/single/output_{i}.png")
            print(f'image saved in output_{i}.png')
            i+=1
            
    else:
        p_idx = 1
        for prompt in prompt_from_gpt:
            prompt_L = []
            prompt_L.append(prompt)
            prompt_L = prompt_L * num_images
            images = pipe(
                prompt_L,
                num_inference_steps = num_inference_steps,
                guidance_scale = guidance_scale,
            ).images

            p_idx += 1
            for idx in range(0, len(images)):
                images[idx].save(f"/content/drive/MyDrive/vscode_python/gpt&diffusion/output/multi/prompt_{p_idx}_output_{idx}.png")
                
    

