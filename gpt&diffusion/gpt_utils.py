import torch
import numpy as np
from transformers import GPT2Tokenizer,GPT2LMHeadModel,GPT2Config
from torch.cuda.amp import GradScaler,autocast
import transformers
from transformers import GPT2Config
from torch.utils.data import Dataset
from torch.utils.data import Dataset,DataLoader
import os
import json
import csv
from transformers import AdamW,get_linear_schedule_with_warmup
from sklearn.model_selection import train_test_split
import torch, os, pandas as pd, json
from sklearn.model_selection import train_test_split
from transformers import  GPT2Tokenizer, GPT2LMHeadModel
import time
import datetime
import random
from torch.cuda.amp import autocast, GradScaler
import argparse

device = 'cuda'

def gpt2_loader(mode = 'pretrained',gpt2_type = 'gpt2-medium', device = 'cuda', use_default = False):
    assert mode == 'pretrained' or mode == 'trained' ,'mode cant be pretrained or trained'

    if mode == 'pretrained':
        print('using pretrained')
        config = GPT2Config.from_pretrained(gpt2_type)
        
        tokenizer = GPT2Tokenizer.from_pretrained(gpt2_type)
        
        model = GPT2LMHeadModel.from_pretrained(gpt2_type, config = config)
        
        
        model = model.to(device)
        return model, tokenizer

    elif mode == 'trained':
        print('using trained')
        if use_default:
            print('using default path of your own model')
            tokenizer_path = '/content/drive/MyDrive/gpt-medium/grim_10-50_5e-5/save_pretrained_30'
            config_path = '/content/drive/MyDrive/gpt-medium/grim_10-50_5e-5/save_pretrained_30/config.json'
            model_path = '/content/drive/MyDrive/gpt-medium/grim_10-50_5e-5/save_pretrained_30/pytorch_model.bin'
        else:
            tokenizer_path = input("tokenizer_path: ")
            config_path = input("config_path: ")
            model_path = input("model_path: ")
        
        tokenizer = GPT2Tokenizer.from_pretrained(tokenizer_path)
        model_config = GPT2Config.from_json_file(config_path)
        model = GPT2LMHeadModel.from_pretrained(model_path,config = model_config)

        model = model.to(device)

        return model, tokenizer
    
    

def gpt2_gen(model, tokenizer, prompt = 'The gpt model is',
            max_length = 100,
            min_length = 20, 
            top_k = 50, 
            top_p = 0.90,
            temperature = 0.7,
            repetition_penalty = 1.5,
            num_return_sequences = 3,
            num_beams = 10,
            num_beam_groups = 10, 
            no_repeat_ngram_size = 2,
            diversity_penalty = 1.0,
            early_stopping = False,
            do_sample = True,
            origin_gen = False):

    #assert output_dir != '','output_dir cant be null'
    
    device = 'cuda'
    model.eval()
    model.to(device)
    prompt = input('your prompt: ')
    input_ids = torch.tensor(tokenizer.encode(prompt)).unsqueeze(0).to(device)
    if origin_gen:
        print('origin mode...')
        generated = model.generate(
            input_ids,
            max_length = 550,
            min_length = 450, 
            top_k = 100, 
            #top_p = 0.50,
            #temperature = 0.7,
            #repetition_penalty = 1.0,
            #num_return_sequences = 1,
            do_sample = True,
            ##################
            # num_beams = 10, 
            # no_repeat_ngram_size = 2,
            # num_beam_groups = 5,
            # diversity_penalty = 1.0,
        ).to(device)

        text_from_gpt = []
        for i, text in enumerate(generated):
            print(tokenizer.decode(text, skip_special_tokens = True))
            text_from_gpt.append(tokenizer.decode(text, skip_special_tokens = True))

        return text_from_gpt
    elif do_sample == True:
        print('do sample...')
        generated = model.generate(
            input_ids,
            max_length = max_length,
            min_length = min_length, 
            num_return_sequences = num_return_sequences,
            top_k = top_k, 
            top_p = top_p,
            temperature = temperature,
            do_sample = do_sample,           
        ).to(device)

        text_from_gpt = []
        for i, text in enumerate(generated):
            print(tokenizer.decode(text, skip_special_tokens = True))
            text_from_gpt.append(tokenizer.decode(text, skip_special_tokens = True))

        return text_from_gpt

    ###====beam====
    else:
        print('beam...')
        generated = model.generate(
            input_ids,
            max_length = max_length,
            min_length = min_length, 
            num_return_sequences = num_return_sequences,
            #num_beams = 10, 
            no_repeat_ngram_size = no_repeat_ngram_size,
            num_beam_groups = num_beam_groups,
            diversity_penalty = diversity_penalty,
        ).to(device)

        text_from_gpt = []
        for i, text in enumerate(generated):
            print(tokenizer.decode(text, skip_special_tokens = True))
            text_from_gpt.append(tokenizer.decode(text, skip_special_tokens = True))
            
        return text_from_gpt
#depricated
def split_story_test(text_from_gpt, comma_split = 3): 
    decode_list = []
    count = 0
    out_list = []
    str_list = []
    for text in text_from_gpt:
        print(text)
        tt = text.split(',')
        print(tt)
        for i in range(0, len(tt), comma_split):
            string = " ".join(tt[i:i+comma_split])
            out_list.append(string)
    return out_list 


    
    





























































































































































































































































































