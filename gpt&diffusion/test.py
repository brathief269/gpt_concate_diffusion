import argparse
from official_diff_utils import *
parser = argparse.ArgumentParser()
parser.add_argument(
    '--model_id',default='CompVis/stable-diffusion-v1-4', type = str, 
    required=False, help='model id offic'
)
args = parser.parse_args()
if __name__ == '__main__':
    print(f'model id: {args.model_id}')
    pipe = official_diff_loader(model_id = args.model_id)
    official_diff_gen(prompt_from_gpt = ['Alice sit by the river'], pipe = pipe)