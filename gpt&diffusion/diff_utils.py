from textwrap import wrap
import os
import keras_cv
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow.experimental.numpy as tnp
from keras_cv.models.stable_diffusion.clip_tokenizer import SimpleTokenizer
from keras_cv.models.stable_diffusion.diffusion_model import DiffusionModel
from keras_cv.models.stable_diffusion.image_encoder import ImageEncoder
from keras_cv.models.stable_diffusion.noise_scheduler import NoiseScheduler
from keras_cv.models.stable_diffusion.text_encoder import TextEncoder
from tensorflow import keras

device = 'cuda'

def diff_loader(url = '', img_height = 256, img_width = 256):
    assert url != '','url cant be null'

    weights_path = tf.keras.utils.get_file(
        origin = url
    )

    diff_model = keras_cv.models.StableDiffusion(
        img_width=img_width, img_height=img_height
    )

    diff_model.diffusion_model.load_weights(weights_path)

    return diff_model
    
def diff_gen(diff_model, text_from_gpt = [], num_image_gen = 3, unconditional_guidance_scale = 40):
    import cv2
    #cv2.imwrite(path,img_to_save)
    from PIL import Image

    prompts = text_from_gpt
    outputs = {}
    
    for prompt in prompts:
        print(f'prompt: {prompt}')
        generated_images = diff_model.text_to_image(
            prompt, batch_size=num_image_gen, unconditional_guidance_scale=unconditional_guidance_scale
        )
        for i in range(len(generated_images)):
            im = Image.fromarray(generated_images[i])
            im.save(f"/content/drive/MyDrive/vscode_python/gpt&diffusion/output/others/output_{i+1}.png")
        outputs.update({prompt: generated_images})

    # def plot_images(images, title):
    #     plt.figure(figsize=(10, 10))
    #     for i in range(len(images)):
    #         ax = plt.subplot(1, len(images), i + 1)
    #         plt.imshow(images[i])
    #         #plt.title(title, fontsize=12)
    #         im = Image.fromarray(images[i])
    #         im.save(f"output_{i}.png")
    #         plt.axis("off")

    # for prompt in outputs:
    #     print('image prompt: ',prompt)
    #     plot_images(outputs[prompt], prompt)


