from gpt_utils import *
from sum_utils import *

import argparse, os, math
device = 'cuda'

parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpt_mode',default='pretrained', type = str, 
    required=True,
)
parser.add_argument(
    '--diff_mode',default='pretrained', type = str, 
    required=True,
)
parser.add_argument(
    '--use_diff',action = 'store_true'
)
parser.add_argument(
    '--model_id',default='runwayml/stable-diffusion-v1-5', type = str, 
    required=False,
)
parser.add_argument(
    '--reso',default=512, type = int, 
    required=False, help='resolution for diffusion'
)
parser.add_argument(
    '--do_sample', action = 'store_true', help='sample or beam'
)
parser.add_argument(
    '--sum_deno',default=1, type = int, 
    required=False, help='denominator of summary multi'
)
parser.add_argument(
    '--sum_mole',default=2, type = int, 
    required=False, help='molecular of summary multi'
)
parser.add_argument(
    '--text_gen_num',default=1, type = int, 
    required=False, help='text_gen_num'
)
parser.add_argument(
    '--img_gen_num',default=1, type = int, 
    required=False, help='img_gen_num'
)
parser.add_argument(
    '--use_default', action = 'store_true'
)
parser.add_argument(
    '--use_sum_short_iter', action = 'store_true'
)
parser.add_argument(
    '--sum_or_not', action = 'store_true'
)
parser.add_argument(
    '--sum_short_min',default=40, type = int, 
    required=False, help='sum_short_min'
)

args = parser.parse_args()
if __name__ == '__main__':
    gpt2_model, gpt2_tokenizer = gpt2_loader(mode = args.gpt_mode, use_default = args.use_default)

    print('----------GPT ready to go----------')

    # sum_model, sum_tokenizer = sum_model_loader()

    # print('----------sum_model ready to go----------')
    if args.use_diff:
        if args.diff_mode == 'trained':
            from diff_utils import *
            diff_model = diff_loader(
                    url = 'https://huggingface.co/datasets/Smoden/ALICE_IMAGE_DATASET/resolve/main/Alice_Fine_Tuned.h5',
                    img_height = args.reso, img_width = args.reso
                )

        elif args.diff_mode == 'pretrained':    
            from official_diff_utils import *
            print(f'model id: {args.model_id}')
            pipe = official_diff_loader(model_id = args.model_id)
        
    print('----------diff_model ready to go----------')

    while True:
        print('ready to gen.....')
        check = input('command(exit, gen): ')
        if check == 'exit':
            os._exit(0)
        if check == 'gen':
            text_from_gpt = gpt2_gen(
                model = gpt2_model,
                tokenizer = gpt2_tokenizer,
                max_length = 100,
                min_length = 20,
                num_return_sequences = args.text_gen_num,
                top_k = 50,
                top_p = 0.95,
                temperature = 0.7,
                do_sample = True,
                origin_gen = True,
                #repetition_penalty = 1,
                #no_repeat_ngram_size = 2,
                #num_beams = 10,
                #num_beam_groups = 10,
                #diversity_penalty = 1.0,
                #early_stopping = False,
            )
            i = 0
            sum_dict = {}
            for t in text_from_gpt:
                words = len(t.split())
                print('words:',words)
                sum_factor = math.ceil(words / args.sum_mole * 1) / 1.0 * args.sum_deno      
                sum_dict.update({i: sum_factor})
                i += 1
            #input('confirm:(press any key) ')

            sum_list = []
            if args.sum_or_not:
                sum_list = summarize_period(text_from_gpt, 
                        sum_model, 
                        sum_tokenizer, 
                        sum_dict = sum_dict,
                )

                if args.use_sum_short_iter:
                    sum_list = sum_short_iter(sum_list, 
                        sum_model, 
                        sum_tokenizer, 
                        sum_short_min = args.sum_short_min,
                    )
            else:
                print('didnt sum')
                sum_list.extend( [x for x in text_from_gpt[0].split('.') if x] )
                
            #print(f'sum list:{sum_list}')
            with open('/content/drive/MyDrive/vscode_python/gpt&diffusion/output/single/story_output.txt','w') as f:
                for i, sen in zip(range(0,len(sum_list)), sum_list):
                    print(f'{i}:{sen}')
                    
            if args.use_diff:
                if args.diff_mode == 'pretrained':
                    official_diff_gen(prompt_from_gpt = sum_list, 
                        pipe = pipe,
                        num_inference_steps = 30,
                        guidance_scale = 7,
                        multi_img = False,
                        num_images = 1,
                    )

                elif args.diff_mode == 'trained':
                    diff_gen(diff_model, 
                        text_from_gpt = sum_list, 
                        num_image_gen = args.img_gen_num, 
                        unconditional_guidance_scale = 40,
                    )


