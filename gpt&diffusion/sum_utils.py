from transformers import AutoTokenizer, AutoModelForSeq2SeqLM

device = 'cuda'

def sum_model_loader():
    tokenizer = AutoTokenizer.from_pretrained("knkarthick/MEETING_SUMMARY")
    model = AutoModelForSeq2SeqLM.from_pretrained("knkarthick/MEETING_SUMMARY")
    
    return model, tokenizer

def summarize_period(text_from_gpt, sum_model, sum_tokenizer, sum_dict = {}):
    assert sum_dict != {}, 'sum_dict can not be null'
    print('using summarize_period mode')
    sum_model.eval()
    sum_model = sum_model.to('cuda')
    sum_list = []
    for text, sum_min in zip(text_from_gpt, sum_dict.values()):
        input_ids = sum_tokenizer.encode(text, return_tensors="pt").to('cuda')
        print(f'miniest summary gen len:{sum_min}')
        generated_sequence = sum_model.generate(
            input_ids = input_ids,
            #no_repeat_ngram_size = 3,
            #num_beams = 20,
            #num_beam_groups = 2,
            max_length = (int(sum_min) + 5),
            min_length = int(sum_min),
            #temperature = 1.0,
            #top_k = 20,
            #top_p = 0.95,
            #do_sample = True,
        ).to('cuda')
        output_text = sum_tokenizer.decode(generated_sequence.squeeze(), skip_special_tokens=True)
        print(output_text)
        sum_list.extend( [x+',' for x in output_text.split('.') if x] )

    return sum_list

def sum_short_iter(text_from_sum, sum_model, sum_tokenizer, sum_short_min = 30):
    #assert sum_short_min > 30,'sum_short_min has to be > 30'
    print('using sum_short_iter mode')
    sum_model.eval()
    sum_model = sum_model.to('cuda')
    last_sum = ''
    final_sum = []
    delimiter = '.'
    for current_s in text_from_sum:   
        need_check_str = last_sum + current_s
        
        need_check_str_len = len(need_check_str.split())
        
        if need_check_str_len > 30 :
            
            input_ids = sum_tokenizer.encode(need_check_str, return_tensors="pt").to('cuda')
            generated_sequence = sum_model.generate(
                input_ids = input_ids,
                #no_repeat_ngram_size = 3,
                #num_beams = 20,
                #num_beam_groups = 2,
                #max_length = sum_short_min, #this is not the true length of output words
                min_length = sum_short_min,
                #temperature = 1.0,
                #top_k = 20,
                #top_p = 0.95,
                #do_sample = True,
            ).to('cuda')
            output_text = sum_tokenizer.decode(generated_sequence.squeeze(), skip_special_tokens=True)
            
            final_sum.append(output_text)

            def cut_it(output_text):
                for i in range(1,100):
                    if output_text.split('.')[-i]:
                        cut_the_sum = output_text.split(',')[-i]
                        

                        if cut_the_sum[-1] != ',' and cut_the_sum[-1] != '.':
                            cut_the_sum += ','

                        return cut_the_sum
            

            last_sum = ''
            #print('last_sum:',last_sum)
        else:
            #final_sum.append(need_check_str)
            last_sum = need_check_str
    return final_sum



